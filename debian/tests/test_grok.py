import re
import sys

sys.dont_write_bytecode = True

def test_grok_date(host):
    cmd = host.run("grok -f grok/date.grok")
    assert cmd.rc == 0
    assert re.search(r"@MATCH", cmd.stdout, flags=re.MULTILINE)

def test_grok_hosts(host):
    cmd = host.run("grok -f grok/hosts.grok")
    assert cmd.rc == 0
    assert re.search(r"localhost", cmd.stdout, flags=re.MULTILINE)

